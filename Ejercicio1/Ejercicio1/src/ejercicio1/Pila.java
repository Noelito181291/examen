/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Alvasro Noel Ventura ---  CI 8655591   --- RU 85948
 */
public class Pila {
    String arr[];
    int maxSize;
    int top;
    
    public Pila(int n){ 
        maxSize = n;
        arr = new String[maxSize];
        top = 0;
    }
    public boolean Empty(){
        if(top == 0){
            return true;
        }else{
            return false;
        }
    }
    public boolean Isfull(){
        if(top == maxSize){
            return true;
        }else{
            return false;
        }
    }
    public String Peek(){
        if(top > 0 ){
            return arr[top-1];
        }else{
            return null;
        }
    }
    public static boolean IsNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    public void AgregarInt(){
        Scanner teclado=new Scanner(System.in);
        String str;
        str=teclado.next();
        if (IsNumeric(str)==true){
            if(top < maxSize){
                arr[top] = str;
                top++;
                System.out.println("Dato agregado");
            }else{
                System.out.println("Ya no caben mas datos");
            }
        }else{
            System.out.println("Porfavor ingrese solo Numeros");
            AgregarInt();
        }
    }
    public void AgregarArr(){
        Scanner teclado=new Scanner(System.in);
        String str;
        str=teclado.next();
        if (IsNumeric(str)==false){
            if(top < maxSize){
                arr[top] = str;
                top++;
                System.out.println("Dato agregado");
            }else{
                System.out.println("Ya no caben mas datos");
            }
        }else{
            System.out.println("Porfavor ingrese solo Letras");
            AgregarArr();
        }
    }
    public void Agregar(){
        Scanner teclado=new Scanner(System.in);
        String str;
        str=teclado.next();
        if(top < maxSize){
            arr[top] = str;
            top++;
            System.out.println("Dato agregado");
        }else{
            System.out.println("Ya no caben mas datos");
        }
    }
    public void Agregar2(String str){
        if(top < maxSize){ 
            arr[top] = str;
            top++;
        }
    }
    public String Eliminar(){
        String temp = null;
        if(top>0){
            temp = arr[top-1];
            arr[top-1] = null;
            top--;
            System.out.println("Dato eliminado");
        }return temp;
    }
    public void Espacio(){
        int a=(maxSize-top);
        System.out.println("Existen "+top+" elementos en la pila");
        System.out.println("Existen "+a+" espacios libres en la pila");
    }
    public void Maxmin(){
        String s;
        String aux3="";
        String aux4="";
        int aux1=0;
        int aux2=256;
        if(!Empty()){
            for(int a=(top-1);a>=0;a--){
                s=arr[a];
                char f = s.charAt(0);
                int ascii = (int) f;
                int x=ascii;
                if(x>=aux1){
                    aux1=x;
                    aux3=arr[a];
                }
                if(x<=aux2){
                    aux2=x;
                    aux4=arr[a];
                }
            }
            System.out.println("el elemento mayor es "+aux3);
            System.out.println("el elemento menor es "+aux4);
        }else System.out.println("Pila vacia");
    }
    public boolean Buscar(String dato){
        boolean encontrado=false;
        if(!Empty()){
            for(int a=(top-1);a>=0;a--){
                if(arr[a].equals(dato)) encontrado=true;
            }
        }
        else System.out.println("No hay datos para buscar en la pila");
        return encontrado;
    }
    public void Ordenar(){
        
        /*char array[];
        array=new char[7];
        char[] numeros = {7,'a',5,'g',1,10,8};
        Arrays.sort(numeros);
        for (int i=0;i<7;i++)
        System.out.println(numeros[i]);*/
    }
    public void Mostrar(){
        if(!Empty()){
            for(int a=(top-1);a>=0;a--){
                System.out.println(arr[a]);
            }
        }else System.out.println("Pila vacia");
    }
}
