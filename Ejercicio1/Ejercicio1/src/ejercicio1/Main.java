/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Alvasro Noel Ventura ---  CI 8655591   --- RU 85948
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n,opcion;
        int tipo=0;
        String dato;
        Scanner teclado=new Scanner(System.in);
        System.out.println("Tipo de dato para la pila ");
        System.out.println("1. Solo numeros");
        System.out.println("2. Solo Letras");
        System.out.println("3. Ambos");
        System.out.println("Elija una opcion ");
        opcion=teclado.nextInt();
        switch(opcion){
            case 1:
                tipo=1;
            break;
            case 2:
                tipo=2;
            break;
            case 3:
                tipo=3;
            break;
        }
        System.out.println("**************************************************");
        System.out.println("Tamanio para la pila: ");
        n=teclado.nextInt();
        Pila pilita=new Pila(n);
        System.out.println("**************************************************");
        do{
            System.out.println("1. Agregar elemento");
            System.out.println("2. Eliminar elemento");
            System.out.println("3. Mostrar espacio");
            System.out.println("4. Mostrar maximo y minimo");
            System.out.println("5. Buscar elemento");
            System.out.println("6. Ordenar elementos");
            System.out.println("7. Mostrar elementos");
            System.out.println("8. Salir");
            System.out.println("Elija una opcion ");
            opcion=teclado.nextInt();
            System.out.println("**************************************************");
            switch(opcion){
                case 1:
                    System.out.println("Escriba un dato: ");
                    if(tipo==1){
                        pilita.AgregarInt();
                    }else if(tipo==2){
                        pilita.AgregarArr();
                    }else if(tipo==3){
                        pilita.Agregar();
                    }
                    System.out.println("**************************************************");
                break;
                case 2:
                    pilita.Eliminar();
                    System.out.println("**************************************************");
                break;
                case 3:
                    pilita.Espacio();
                    System.out.println("**************************************************");
                break;
                case 4:
                    pilita.Maxmin();
                    System.out.println("**************************************************");
                break;
                case 5:
                    System.out.println("Dato a buscar: ");
                    dato=teclado.next();
                    if(pilita.Buscar(dato)) System.out.println("Dato encontrado");
                    else System.out.println("Dato no encontrado");
                    System.out.println("**************************************************");
                break;
                case 6:
                    List datos = new LinkedList<>();
                    int o=0;
                    for(int i=0;!pilita.Empty();i++){
                        datos.add(pilita.Eliminar());
                        o++;
                    }
                    Collections.sort(datos);
                    for(int i=0;i<o;i++){
                        pilita.Agregar2(datos.get(i).toString());
                    }
                    System.out.println("**************************************************");
                break;
                case 7:
                    pilita.Mostrar();
                    System.out.println("**************************************************");
                break;
            }
        }while(opcion!=8);
    }
    
}
