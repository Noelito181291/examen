/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

/**
 *
 * @author Alvasro Noel Ventura ---  CI 8655591   --- RU 85948
 */
public class Verificador {
    String mycad;
    Pila stackval = new Pila();
    
    public Verificador(String str){
        this.mycad = str;
    }
    
    public void checkStackVal(String strs){   
        for(int i = 0;i<strs.length();i++ ){
            if(strs.charAt(i) == '('){
                stackval.push(')');
            }else if(strs.charAt(i) == '['){
                stackval.push(']');
            }else if(strs.charAt(i) == ']'){
                checkStack(']','[');
            }else if(strs.charAt(i) == ')'){
                checkStack(')','(');
            }
        }
        if(stackval.isEmpty()){
            System.out.println("La cadena esta valanceada");
        }else{
            System.out.println("La cadena no esta valanceada");
        }
    }  
    public void checkStack(char c, char cc){
        if(stackval.isEmpty()){
            System.out.println("no valanceado" + cc);
            System.exit(0);
        }else{
            char s = stackval.pop();
            if(c != s){
                System.out.println("no valanceado" + s);    
                System.exit(0);
            }
        }
    }
}
